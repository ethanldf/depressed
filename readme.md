# Digital Depression Detector

A lightweight mobile depression detector meant for conducting a loose diagnosis for depression, as well as promoting depression awareness.

There is already a compiled apk called depressdetect.apk in the project folder.

This app is built for android, ios is not supported.

#### Dependencies (for compiling)
* Node.js 12.13.1
* npm 6.12.1
* Ionic 4.7.1
* Cordova 9.0.0
* AmCharts 4.7.11
* Java SE Development Kit 8 (Ionic can only compile with jdk 8)
* Android Studio 3.5.2 (for Android)
* Xcode (for Apple IOS, not tested)

#### SETUP 
Install latest LTS of Node.js from https://nodejs.org/en/download/

Install Ionic
```sh
npm install ionic
```

Install required dependencies fron package.json in current folder
```sh
npm install
```

### Compiling for Android
Make sure Android Studio is installed.

Set up environment variables
```sh
export ANDROID_SDK_ROOT=$HOME/Library/Android/sdk

export PATH=$PATH:$ANDROID_SDK_ROOT/tools/bin

export PATH=$PATH:$ANDROID_SDK_ROOT/platform-tools

export PATH=$PATH:$ANDROID_SDK_ROOT/emulator
```


Compile an unsigned debugging version APK for installation in Android
```sh
ionic cordova build android
```


