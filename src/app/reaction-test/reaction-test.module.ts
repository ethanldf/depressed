import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReactionTestPageRoutingModule } from './reaction-test-routing.module';

import { ReactionTestPage } from './reaction-test.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactionTestPageRoutingModule
  ],
  declarations: [ReactionTestPage]
})
export class ReactionTestPageModule {}
