import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ReactionTestPage } from './reaction-test.page';

describe('ReactionTestPage', () => {
  let component: ReactionTestPage;
  let fixture: ComponentFixture<ReactionTestPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReactionTestPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ReactionTestPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
