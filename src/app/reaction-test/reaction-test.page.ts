import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Vibration } from '@ionic-native/vibration/ngx';
import { DeviceFeedback } from '@ionic-native/device-feedback/ngx';

@Component({
  selector: 'app-reaction-test',
  templateUrl: './reaction-test.page.html',
  styleUrls: ['./reaction-test.page.scss'],
})
export class ReactionTestPage implements OnInit {
  isReact = false;
  dLevel1 : any;
  enableScreenClick = false;
  buttonColor : any;
  tapped : any;
  isTime : any;
  timing : any;

  rt : number;
  start : any;

  constructor(private vibration: Vibration, private router: Router, private route: ActivatedRoute, private deviceFeedback: DeviceFeedback) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.dLevel1 = params.dLevel1; 

      this.buttonColor = "medium";
      this.tapped = false;
      this.isTime = false;
      this.timing = false;

      this.rando();

    });
    
  }

  vibrato() {
    this.buttonColor = "danger";
    this.vibration.vibrate(500);
    this.isReact = true;
    this.isTime = true;
    this.enableScreenClick = true;
    this.start = Date.now();
    
  }

  rando() {
    var rand = Math.floor(Math.random() * 5) + 1;
    
    setTimeout(() => {this.vibrato()}, rand * 1000);
  }

  clicky() {
    const end = Date.now();
    // this.deviceFeedback.haptic(0);
    if (this.timing == false) {
      this.isReact = false;
      this.rt = end - this.start;

      this.tapped = true;
      this.timing = true;
      this.buttonColor = "primary";
    }
    else {
      this.router.navigate(['/reaction-test-result', {rt : this.rt, dLevel1 : this.dLevel1}])
    }
  }
  screenClicky() {
    const end = Date.now();
    if (this.enableScreenClick == true) {
      if (this.timing == false) {
        this.deviceFeedback.haptic(0);
        this.isReact = false;
        this.rt = end - this.start;
  
        this.tapped = true;
        this.timing = true;
        this.buttonColor = "primary";
      }
    }
    
  }

}
