import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { DeviceFeedback } from '@ionic-native/device-feedback/ngx';
import { DBMeter } from '@ionic-native/db-meter/ngx';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private deviceFeedback: DeviceFeedback, private dbMeter: DBMeter) { }

  ngOnInit() {
    let subscription = this.dbMeter.start().subscribe(
      data => {
        subscription.unsubscribe();
      }
    );
    this.dbMeter.delete();
  }

  onClick() { 
    this.deviceFeedback.haptic(0);
    
    this.router.navigate(['/sound-test-guide']);
  }

}
