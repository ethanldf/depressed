import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-depress',
  templateUrl: './depress.page.html',
  styleUrls: ['./depress.page.scss'],
})
export class DepressPage implements OnInit {
  dLevel1 : number;
  dLevel2 : number;
  dLevel3 : number;
  dLevel : number;
  dCat : any;

  constructor(private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.dLevel1 = params.dLevel1;
      this.dLevel2 = params.dLevel2;
      this.dLevel3 = params.dLevel3;

      this.dLevel = (this.dLevel1*1) + (this.dLevel2*1) + (this.dLevel3*1);

      if(this.dLevel==9) {
        this.dCat = "High";
      }
      else if(this.dLevel>=6) {
        this.dCat = "Medium";
      }
      else {
        this.dCat = "Low";
      }
    });
  }

}
