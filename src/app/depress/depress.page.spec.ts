import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DepressPage } from './depress.page';

describe('DepressPage', () => {
  let component: DepressPage;
  let fixture: ComponentFixture<DepressPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepressPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DepressPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
