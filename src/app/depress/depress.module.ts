import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DepressPageRoutingModule } from './depress-routing.module';

import { DepressPage } from './depress.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DepressPageRoutingModule
  ],
  declarations: [DepressPage]
})
export class DepressPageModule {}
