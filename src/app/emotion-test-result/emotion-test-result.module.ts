import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EmotionTestResultPageRoutingModule } from './emotion-test-result-routing.module';

import { EmotionTestResultPage } from './emotion-test-result.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EmotionTestResultPageRoutingModule
  ],
  declarations: [EmotionTestResultPage]
})
export class EmotionTestResultPageModule {}
