import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EmotionTestResultPage } from './emotion-test-result.page';

const routes: Routes = [
  {
    path: '',
    component: EmotionTestResultPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EmotionTestResultPageRoutingModule {}
