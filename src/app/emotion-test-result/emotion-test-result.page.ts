import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DeviceFeedback } from '@ionic-native/device-feedback/ngx';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";

@Component({
  selector: 'app-emotion-test-result',
  templateUrl: './emotion-test-result.page.html',
  styleUrls: ['./emotion-test-result.page.scss'],
})
export class EmotionTestResultPage implements OnInit {

  rt1: number;
  rt2: number;
  rt: number;

  dLevel1: number;
  dLevel2: number;

  lineLow = 643 + 109;
  lineHigh = 656 + 114;
  center = 761;

  dLevel3: number;



  constructor(private router: Router, private route: ActivatedRoute, private deviceFeedback: DeviceFeedback) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.rt1 = params.rt1;
      this.rt2 = params.rt2;
      this.dLevel1 = params.dLevel1;
      this.dLevel2 = params.dLevel2;
      this.rt = ((this.rt1 * 1) + (this.rt2 * 1)) / 2;

      if (this.rt <= this.lineLow) {
        this.dLevel3 = 1;
      }
      else if (this.rt >= this.lineHigh) {
        this.dLevel3 = 3;
      }
      else {
        this.dLevel3 = 2;
      }
      try {
        this.draw();
      }
      catch (error) {
        console.log(error);
      }
      // this.draw();
    });

  }

  draw() {

    am4core.disposeAllCharts();
    am4core.useTheme(am4themes_animated);

    /* Create chart instance */
    var chart2 = am4core.create("chartdiv2", am4charts.RadarChart);

    /* Add data */
    chart2.data = [{
      "type": "Average",
      "base": 761,
      "yours": this.rt
    }, {
      "type": "Happy",
      "base": 761,
      "yours": this.rt1
    }, {
      "type": "Sad",
      "base": 761,
      "yours": this.rt2
    }];

    /* Create axes */
    var categoryAxis = chart2.xAxes.push(new am4charts.CategoryAxis() as any);
    categoryAxis.dataFields.category = "type";

    var valueAxis = chart2.yAxes.push(new am4charts.ValueAxis() as any);
    valueAxis.renderer.axisFills.template.fill = chart2.colors.getIndex(2);
    valueAxis.renderer.axisFills.template.fillOpacity = 0.05;

    /* Create and configure series */
    var series = chart2.series.push(new am4charts.RadarSeries());
    series.dataFields.valueY = "base";
    series.dataFields.categoryX = "type";
    series.name = "Base";
    series.strokeWidth = 3;
    series.fillOpacity = 0.5;
    series.fill = chart2.colors.getIndex(1);
    series.strokeOpacity = 0;
    series.sequencedInterpolation = true;
    series.sequencedInterpolationDelay = 50;

    /* Create and configure series */
    var series2 = chart2.series.push(new am4charts.RadarSeries());
    series2.dataFields.valueY = "yours";
    series2.dataFields.categoryX = "type";
    series2.name = "Yours";
    series2.strokeWidth = 3;
    series2.fillOpacity = 0.5;
    series2.fill = chart2.colors.getIndex(6);
    series2.strokeOpacity = 0;
    series2.sequencedInterpolation = true;
    series2.sequencedInterpolationDelay = 50;

    chart2.legend = new am4charts.Legend();
  }

  goNext() {
    am4core.disposeAllCharts();
    this.deviceFeedback.haptic(0);
    this.router.navigate(['/worlddepressed', { dLevel1: this.dLevel1, dLevel2: this.dLevel2, dLevel3: this.dLevel3 }]);
  }
}
