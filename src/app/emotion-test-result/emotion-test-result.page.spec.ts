import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EmotionTestResultPage } from './emotion-test-result.page';

describe('EmotionTestResultPage', () => {
  let component: EmotionTestResultPage;
  let fixture: ComponentFixture<EmotionTestResultPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmotionTestResultPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EmotionTestResultPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
