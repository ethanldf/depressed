import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ReactionTestResultPage } from './reaction-test-result.page';

describe('ReactionTestResultPage', () => {
  let component: ReactionTestResultPage;
  let fixture: ComponentFixture<ReactionTestResultPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReactionTestResultPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ReactionTestResultPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
