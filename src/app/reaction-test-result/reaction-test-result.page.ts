import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DeviceFeedback } from '@ionic-native/device-feedback/ngx';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";

@Component({
  selector: 'app-reaction-test-result',
  templateUrl: './reaction-test-result.page.html',
  styleUrls: ['./reaction-test-result.page.scss'],
})
export class ReactionTestResultPage implements OnInit {
  ctx : any;

  rt : number;
  handRt : number;
  value : number;

  dLevel1 : any;

  lineLow = 321 + 103;
  lineHigh = 337 + 122;
  center = 441;
  max = 882;
  dLevel2: number;

  chart1 : any;

  constructor(private router: Router, private route: ActivatedRoute, private deviceFeedback: DeviceFeedback) { }

  ngOnInit() {  
    this.ctx = document.getElementById("barChart");
    this.rt = 250;
    this.route.params.subscribe(params => {
      this.rt = params.rt;
      if(this.rt >= 500) {
        this.handRt = 500;
      }
      else {
        this.handRt = this.rt;
      }
      this.value = this.rt;
      this.dLevel1 = params.dLevel1;

      if (this.rt <= this.lineLow) {
        this.dLevel2 = 1;
      }
      else if (this.rt >= this.lineHigh) {
        this.dLevel2 = 3;
      }
      else {
        this.dLevel2 = 2;
      }
      
      try {
        this.draw(this.rt, this.handRt);
        // this.draw();
      }
      catch (error) {
        console.log(error);
      }

    });
  }


  draw(rt: number, handRt: number) {
    am4core.disposeAllCharts();
    am4core.useTheme(am4themes_animated);

    // create chart
    let chart1 = am4core.create("chartdiv1", am4charts.GaugeChart);
    this.chart1 = chart1;
    // chart1.handleResize();
    chart1.hiddenState.properties.opacity = 0; // this makes initial fade in effect
    chart1.innerRadius = am4core.percent(82);

    let axis = chart1.xAxes.push(new am4charts.ValueAxis() as any);
    axis.min = 0;
    axis.max = 500;
    axis.strictMinMax = true;
    axis.renderer.radius = am4core.percent(80);
    axis.renderer.inside = true;
    axis.renderer.line.strokeOpacity = 1;
    axis.renderer.ticks.template.disabled = false;
    axis.renderer.ticks.template.strokeOpacity = 1;
    axis.renderer.ticks.template.length = 10;
    axis.renderer.grid.template.disabled = true;
    axis.renderer.labels.template.radius = 40;
    // axis.render.minGridDistance = 1;
    
    axis.renderer.grid.template.stroke = new am4core.InterfaceColorSet().getFor("background");
    axis.renderer.minGridDistance = 200;

    var axis2 = chart1.xAxes.push(new am4charts.ValueAxis() as any);
    axis2.min = 0;
    axis2.max = 500;
    axis2.renderer.innerRadius = 5
    axis2.strictMinMax = true;
    axis2.renderer.labels.template.disabled = true;
    axis2.renderer.ticks.template.disabled = true;
    axis2.renderer.grid.template.disabled = true;
    // axis2.render.minGridDistance = 1;

    let range0 = axis2.axisRanges.create();
    range0.value = 0;
    range0.endValue = 388;
    range0.axisFill.fillOpacity = 1;
    range0.axisFill.fill = am4core.color("#006633");
    range0.axisFill.zIndex = -1;

    let range1 = axis2.axisRanges.create();
    range1.value = 389;
    range1.endValue = 424;
    range1.axisFill.fillOpacity = 1;
    range1.axisFill.fill = am4core.color("#6aff5f");
    range1.axisFill.zIndex = -1;

    let range2 = axis2.axisRanges.create();
    range2.value = 425;
    range2.endValue = 458;
    range2.axisFill.fillOpacity = 1;
    range2.axisFill.fill = am4core.color("#ffc75f");
    range2.axisFill.zIndex = -1;

    let range3 = axis2.axisRanges.create();
    range3.value = 459;
    range3.endValue = 494;
    range3.axisFill.fillOpacity = 1;
    range3.axisFill.fill = am4core.color("#ff5f5f");
    range3.axisFill.zIndex = -1;

    let range4 = axis2.axisRanges.create();
    range4.value = 495;
    range4.endValue = 500;
    range4.axisFill.fillOpacity = 1;
    range4.axisFill.fill = am4core.color("#990000");
    range4.axisFill.zIndex = -1;

    let label = chart1.radarContainer.createChild(am4core.Label);
    label.isMeasured = false;
    label.fontSize = 20;
    label.x = am4core.percent(50);
    label.y = am4core.percent(100);
    label.horizontalCenter = "middle";
    label.verticalCenter = "bottom";

    let hand = chart1.hands.push(new am4charts.ClockHand());
    hand.axis = axis2;
    hand.fill = am4core.color("#845EC2");
    hand.stroke = am4core.color("#845EC2");
    hand.innerRadius = am4core.percent(40);
    hand.pin.disabled = true;
    hand.startWidth = 10;
    console.log(rt);
    // hand.value = rt*1;

    // label.text = rt + " ms";


    setInterval(function () {
      // var value = rt;
      // var value = 400;
      label.text = rt + " ms";
      var animation = new am4core.Animation(hand, {
        property: "value",
        to: handRt*1
      }, 1000, am4core.ease.cubicOut).start();
    }, 1020);
    
  }


  goNext() {
    this.deviceFeedback.haptic(0);
    this.router.navigate(['/emotion-test-guide', { dLevel1: this.dLevel1, dLevel2: this.dLevel2 }]);
  }
}
