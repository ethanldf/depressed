import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReactionTestResultPageRoutingModule } from './reaction-test-result-routing.module';

import { ReactionTestResultPage } from './reaction-test-result.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactionTestResultPageRoutingModule
  ],
  declarations: [ReactionTestResultPage]
})
export class ReactionTestResultPageModule {}
