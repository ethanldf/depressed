import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReactionTestResultPage } from './reaction-test-result.page';

const routes: Routes = [
  {
    path: '',
    component: ReactionTestResultPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReactionTestResultPageRoutingModule {}
