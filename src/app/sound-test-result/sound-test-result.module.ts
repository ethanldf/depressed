import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SoundTestResultPageRoutingModule } from './sound-test-result-routing.module';

import { SoundTestResultPage } from './sound-test-result.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SoundTestResultPageRoutingModule
  ],
  declarations: [SoundTestResultPage]
})
export class SoundTestResultPageModule {}
