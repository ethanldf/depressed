import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DeviceFeedback } from '@ionic-native/device-feedback/ngx';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";

@Component({
  selector: 'app-sound-test-result',
  templateUrl: './sound-test-result.page.html',
  styleUrls: ['./sound-test-result.page.scss'],
})
export class SoundTestResultPage implements OnInit {
  baseline = 690;
  deviation = 270;

  baselineLow = this.baseline - this.deviation;
  baselineHigh = this.baseline + this.deviation;

  spt: any;
  speed: any;
  dLevel1: number;

  chart: any;

  constructor(private router: Router, private route: ActivatedRoute, private deviceFeedback: DeviceFeedback) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.spt = params.spt;

      if (this.spt <= this.baselineLow) {
        this.dLevel1 = 1;
      }
      else if (this.spt >= this.baselineHigh) {
        this.dLevel1 = 3
      }
      else {
        this.dLevel1 = 2;
      }
      var spseries = [
        { value: 690 },
        { value: this.spt }
      ]


      this.draw();
    });
  }

  goNext() {
    this.deviceFeedback.haptic(0);
    am4core.disposeAllCharts();

    // this.chart = null;
    this.router.navigate(['/reaction-test-guide', { dLevel1: this.dLevel1 }]);
  }

  draw() {
    am4core.useTheme(am4themes_animated);

    // Create chart instance
    var chart = am4core.create("chartdiv", am4charts.RadarChart);
    this.chart = chart;

    // Add data
    chart.data = [{
      "category": "Yours",
      "value": this.spt,
      "full": 1380
    }, {
      "category": "Base",
      "value": 690,
      "full": 1380
    }];

    // Make chart not full circle
    chart.startAngle = -90;
    chart.endAngle = 180;
    chart.innerRadius = am4core.percent(10);

    // Set number format
    chart.numberFormatter.numberFormat = "#.#";

    // Create axes
    var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis() as any);
    categoryAxis.dataFields.category = "category";
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.renderer.grid.template.strokeOpacity = 0;
    categoryAxis.renderer.labels.template.horizontalCenter = "right";
    categoryAxis.renderer.labels.template.fontWeight = 100;
    categoryAxis.renderer.labels.template.adapter.add("fill", function (fill, target) {
      return (target.dataItem.index >= 0) ? chart.colors.getIndex(target.dataItem.index) : fill;
    });
    categoryAxis.renderer.minGridDistance = 10;

    var valueAxis = chart.xAxes.push(new am4charts.ValueAxis() as any);
    valueAxis.renderer.grid.template.strokeOpacity = 0;
    valueAxis.min = 0;
    valueAxis.max = 1380;
    valueAxis.strictMinMax = true;

    // Create series
    var series1 = chart.series.push(new am4charts.RadarColumnSeries() as any);
    series1.dataFields.valueX = "full";
    series1.dataFields.categoryY = "category";
    series1.clustered = false;
    series1.columns.template.fill = new am4core.InterfaceColorSet().getFor("alternativeBackground");
    series1.columns.template.fillOpacity = 0.05;
    series1.columns.template.cornerRadiusTopLeft = 20;
    series1.columns.template.strokeWidth = 0;
    series1.columns.template.radarColumn.cornerRadius = 20;

    var series2 = chart.series.push(new am4charts.RadarColumnSeries());
    series2.dataFields.valueX = "value";
    series2.dataFields.categoryY = "category";
    series2.clustered = false;
    series2.columns.template.strokeWidth = 0;
    series2.columns.template.tooltipText = "{category}: [bold]{value}[/]";
    series2.columns.template.radarColumn.cornerRadius = 20;

    series2.columns.template.adapter.add("fill", function (fill, target) {
      return chart.colors.getIndex(target.dataItem.index);
    });

    // Add cursor
    chart.cursor = new am4charts.RadarCursor();
  }

}
