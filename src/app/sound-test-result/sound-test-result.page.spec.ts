import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SoundTestResultPage } from './sound-test-result.page';

describe('SoundTestResultPage', () => {
  let component: SoundTestResultPage;
  let fixture: ComponentFixture<SoundTestResultPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SoundTestResultPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SoundTestResultPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
