import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SoundTestResultPage } from './sound-test-result.page';

const routes: Routes = [
  {
    path: '',
    component: SoundTestResultPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SoundTestResultPageRoutingModule {}
