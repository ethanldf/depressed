import { Component, OnInit } from '@angular/core';
import { TextToSpeech } from '@ionic-native/text-to-speech/ngx';
import { Router, ActivatedRoute } from '@angular/router';
import { DeviceFeedback } from '@ionic-native/device-feedback/ngx';

@Component({
  selector: 'app-reaction-test-guide',
  templateUrl: './reaction-test-guide.page.html',
  styleUrls: ['./reaction-test-guide.page.scss'],
})
export class ReactionTestGuidePage implements OnInit {
  disableBtn : any;
  dLevel1 : any;

  constructor(private tts: TextToSpeech, private router: Router, private route: ActivatedRoute, private deviceFeedback: DeviceFeedback) { }

  ngOnInit() {

    this.route.params.subscribe(params => {
      this.dLevel1 = params.dLevel1; 
      this.disableBtn = true;
      var an = "We will be conducting a reaction test. Press the alert button when you feel a vibration. Please press the button when you are ready."
      this.tts.speak(an).then(() => this.disableBtn = false);

    });

  }

  goNext() {
    this.deviceFeedback.haptic(0);
    this.router.navigate(['/reaction-test', {dLevel1: this.dLevel1}]);
  }

}
