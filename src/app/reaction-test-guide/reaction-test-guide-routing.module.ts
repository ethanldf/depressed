import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReactionTestGuidePage } from './reaction-test-guide.page';

const routes: Routes = [
  {
    path: '',
    component: ReactionTestGuidePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReactionTestGuidePageRoutingModule {}
