import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ReactionTestGuidePage } from './reaction-test-guide.page';

describe('ReactionTestGuidePage', () => {
  let component: ReactionTestGuidePage;
  let fixture: ComponentFixture<ReactionTestGuidePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReactionTestGuidePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ReactionTestGuidePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
