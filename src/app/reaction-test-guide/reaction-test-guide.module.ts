import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReactionTestGuidePageRoutingModule } from './reaction-test-guide-routing.module';

import { ReactionTestGuidePage } from './reaction-test-guide.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactionTestGuidePageRoutingModule
  ],
  declarations: [ReactionTestGuidePage]
})
export class ReactionTestGuidePageModule {}
