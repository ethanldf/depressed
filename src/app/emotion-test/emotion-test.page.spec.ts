import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EmotionTestPage } from './emotion-test.page';

describe('EmotionTestPage', () => {
  let component: EmotionTestPage;
  let fixture: ComponentFixture<EmotionTestPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmotionTestPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EmotionTestPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
