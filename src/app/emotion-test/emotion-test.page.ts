import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DeviceFeedback } from '@ionic-native/device-feedback/ngx';

@Component({
  selector: 'app-emotion-test',
  templateUrl: './emotion-test.page.html',
  styleUrls: ['./emotion-test.page.scss'],
})
export class EmotionTestPage implements OnInit {

  nextBtn : any;
  tested : any;
  imgpath : any;
  qn : any;

  start1 : any;
  start2 : any;

  resultTime1 : any;
  resultTime2 : any;

  dLevel1 : number;
  dLevel2 : number;


  constructor(private router: Router, private deviceFeedback: DeviceFeedback, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.dLevel1 = params.dLevel1;
      this.dLevel2 = params.dLevel2;
    
      this.nextBtn = false;
      this.tested = false;
      this.qn = 0;
      // this.qn = 2;
      this.imgpath = "../../assets/icon/trainhappy.jpg";
      // this.imgpath = "../../assets/icon/happydog.jpg";
      // this.imgpath = "../../assets/icon/saddog.jpg";
      // this.start1 = Date.now();
    });
  }

  answer() {
    var ansTime = Date.now();
    this.deviceFeedback.haptic(0);
    if (this.qn==0) {
      this.qn = 1;
      this.imgpath = "../../assets/icon/trainsad.jpg";
    }
    else if (this.qn==1) {
      this.qn = 2;
      this.imgpath = "../../assets/icon/happydog.jpg";
      this.start1 = Date.now();
    }
    else if ( this.qn == 2) {
        // var ansTime = Date.now();
        this.resultTime1 = ansTime - this.start1;

        this.qn =3;
        this.imgpath = "../../assets/icon/saddog.jpg";
        // this.imgpath = "../../assets/icon/happydog.jpg";
        this.start2 = Date.now();
    }
    else if (this.qn == 3) {
        // var ansTime = Date.now();
        this.resultTime2 = ansTime - this.start2;
        this.nextBtn = true;
        this.tested = true;
    }

  }

  goNext() {
    this.deviceFeedback.haptic(0);
    this.router.navigate(['/emotion-test-result', {rt1: this.resultTime1, rt2: this.resultTime2, dLevel1: this.dLevel1, dLevel2: this.dLevel2}]);
  }

}
