import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EmotionTestPageRoutingModule } from './emotion-test-routing.module';

import { EmotionTestPage } from './emotion-test.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EmotionTestPageRoutingModule
  ],
  declarations: [EmotionTestPage]
})
export class EmotionTestPageModule {}
