import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EmotionTestPage } from './emotion-test.page';

const routes: Routes = [
  {
    path: '',
    component: EmotionTestPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EmotionTestPageRoutingModule {}
