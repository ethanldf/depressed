import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'sound-test-guide',
    loadChildren: () => import('./sound-test-guide/sound-test-guide.module').then( m => m.SoundTestGuidePageModule)
  },
  {
    path: 'sound-test',
    loadChildren: () => import('./sound-test/sound-test.module').then( m => m.SoundTestPageModule)
  },
  {
    path: 'sound-test-result',
    loadChildren: () => import('./sound-test-result/sound-test-result.module').then( m => m.SoundTestResultPageModule)
  },
  {
    path: 'reaction-test-guide',
    loadChildren: () => import('./reaction-test-guide/reaction-test-guide.module').then( m => m.ReactionTestGuidePageModule)
  },
  {
    path: 'reaction-test',
    loadChildren: () => import('./reaction-test/reaction-test.module').then( m => m.ReactionTestPageModule)
  },
  {
    path: 'reaction-test-result',
    loadChildren: () => import('./reaction-test-result/reaction-test-result.module').then( m => m.ReactionTestResultPageModule)
  },
  {
    path: 'emotion-test-guide',
    loadChildren: () => import('./emotion-test-guide/emotion-test-guide.module').then( m => m.EmotionTestGuidePageModule)
  },
  {
    path: 'emotion-test',
    loadChildren: () => import('./emotion-test/emotion-test.module').then( m => m.EmotionTestPageModule)
  },
  {
    path: 'emotion-test-result',
    loadChildren: () => import('./emotion-test-result/emotion-test-result.module').then( m => m.EmotionTestResultPageModule)
  },
  {
    path: 'info',
    loadChildren: () => import('./info/info.module').then( m => m.InfoPageModule)
  },
  {
    path: 'depress',
    loadChildren: () => import('./depress/depress.module').then( m => m.DepressPageModule)
  },
  {
    path: 'worlddepressed',
    loadChildren: () => import('./worlddepressed/worlddepressed.module').then( m => m.WorlddepressedPageModule)
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
