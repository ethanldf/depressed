import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WorlddepressedPage } from './worlddepressed.page';

const routes: Routes = [
  {
    path: '',
    component: WorlddepressedPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WorlddepressedPageRoutingModule {}
