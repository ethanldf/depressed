import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { WorlddepressedPageRoutingModule } from './worlddepressed-routing.module';

import { WorlddepressedPage } from './worlddepressed.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    WorlddepressedPageRoutingModule
  ],
  declarations: [WorlddepressedPage]
})
export class WorlddepressedPageModule {}
