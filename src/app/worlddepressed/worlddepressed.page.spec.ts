import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { WorlddepressedPage } from './worlddepressed.page';

describe('WorlddepressedPage', () => {
  let component: WorlddepressedPage;
  let fixture: ComponentFixture<WorlddepressedPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorlddepressedPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(WorlddepressedPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
