import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { Router } from '@angular/router';

import { TextToSpeech } from '@ionic-native/text-to-speech/ngx';
import { DBMeter } from '@ionic-native/db-meter/ngx';
import { NgZone } from '@angular/core';
// import { Chart } from 'chart.js';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private router : Router,
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    // private chart: Chart
  ) {
    this.initializeApp();
  }

  // initializeApp() {
  //   this.platform.ready().then(() => {
  //     this.statusBar.styleDefault();
  //     this.splashScreen.hide();
  //   });
  // }

  initializeApp() {
    this.platform.ready().then(() => {
      this.router.navigateByUrl('home');
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
