import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SoundTestPage } from './sound-test.page';

describe('SoundTestPage', () => {
  let component: SoundTestPage;
  let fixture: ComponentFixture<SoundTestPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SoundTestPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SoundTestPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
