import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SoundTestPageRoutingModule } from './sound-test-routing.module';

import { SoundTestPage } from './sound-test.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SoundTestPageRoutingModule
  ],
  declarations: [SoundTestPage]
})
export class SoundTestPageModule {}
