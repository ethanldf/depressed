import { Component, OnInit } from '@angular/core';
import { TextToSpeech } from '@ionic-native/text-to-speech/ngx';
import { SpeechRecognition } from '@ionic-native/speech-recognition/ngx';
import { DBMeter } from '@ionic-native/db-meter/ngx';
import { NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { DeviceFeedback } from '@ionic-native/device-feedback/ngx';

@Component({
  selector: 'app-sound-test',
  templateUrl: './sound-test.page.html',
  styleUrls: ['./sound-test.page.scss'],
})
export class SoundTestPage implements OnInit {
  disableBtn : any;
  db = 0;
  isListen = false;

  matches: any[];
  spt : number;


  constructor(private tts: TextToSpeech, private dbMeter: DBMeter, private zone: NgZone, private router: Router, private deviceFeedback: DeviceFeedback) { }

  listen() {
    this.isListen = true;
    const start = Date.now();

    let subscription = this.dbMeter.start().subscribe(
      data => {
        this.zone.run(() => {
          this.db = Number(data);
          // this.matches.push(data);
          // var newDb = this.db;
          
          
          if (this.db > 70) {
            const end = Date.now();
            this.disableBtn = false;
            this.isListen = false;
            // this.spt = Math.abs(end - start);
            this.spt = end - start;

            // console.log('SPT is : ' + this.spt);
            
            subscription.unsubscribe();
          }
          // else
          //   this.matches.push(data);
        });
      }
    );
  }

  goNext() {
    // this.spt = 123;
    this.deviceFeedback.haptic(0);
    this.router.navigate(['/sound-test-result', {spt: this.spt}]);
  }

  ngOnInit() {
    this.disableBtn = true;
    this.tts.speak('Please listen carefully. What did you eat for supper yesterday?').then(() => this.listen()).catch((reason: any) => console.log(reason));
  }


  
}