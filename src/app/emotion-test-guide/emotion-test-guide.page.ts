import { Component, OnInit } from '@angular/core';
import { TextToSpeech } from '@ionic-native/text-to-speech/ngx';
import { Router, ActivatedRoute } from '@angular/router';
import { DeviceFeedback } from '@ionic-native/device-feedback/ngx';

@Component({
  selector: 'app-emotion-test-guide',
  templateUrl: './emotion-test-guide.page.html',
  styleUrls: ['./emotion-test-guide.page.scss'],
})
export class EmotionTestGuidePage implements OnInit {

  disableBtn : any;
  dLevel1 : number;
  dLevel2 : number;

  constructor(private tts: TextToSpeech, private router: Router, private route: ActivatedRoute, private deviceFeedback: DeviceFeedback) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.dLevel1 = params.dLevel1;
      this.dLevel2 = params.dLevel2;
      this.disableBtn = true;
      var textspeech = "We will be conducting a short emotion test. Please select the emotion that you feel best represents the image shown. Please press the button when you are ready."
      this.tts.speak(textspeech).then(() => this.disableBtn = false);
    });
  }

  goNext() {
    this.deviceFeedback.haptic(0);
    this.router.navigate(['/emotion-test', {dLevel1 : this.dLevel1, dLevel2 : this.dLevel2}]);
  }

}
