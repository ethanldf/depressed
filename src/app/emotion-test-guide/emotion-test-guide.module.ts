import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EmotionTestGuidePageRoutingModule } from './emotion-test-guide-routing.module';

import { EmotionTestGuidePage } from './emotion-test-guide.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EmotionTestGuidePageRoutingModule
  ],
  declarations: [EmotionTestGuidePage]
})
export class EmotionTestGuidePageModule {}
