import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EmotionTestGuidePage } from './emotion-test-guide.page';

describe('EmotionTestGuidePage', () => {
  let component: EmotionTestGuidePage;
  let fixture: ComponentFixture<EmotionTestGuidePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmotionTestGuidePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EmotionTestGuidePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
