import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EmotionTestGuidePage } from './emotion-test-guide.page';

const routes: Routes = [
  {
    path: '',
    component: EmotionTestGuidePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EmotionTestGuidePageRoutingModule {}
