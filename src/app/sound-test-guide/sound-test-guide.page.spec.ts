import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SoundTestGuidePage } from './sound-test-guide.page';

describe('SoundTestGuidePage', () => {
  let component: SoundTestGuidePage;
  let fixture: ComponentFixture<SoundTestGuidePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SoundTestGuidePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SoundTestGuidePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
