import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SoundTestGuidePage } from './sound-test-guide.page';

const routes: Routes = [
  {
    path: '',
    component: SoundTestGuidePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SoundTestGuidePageRoutingModule {}
