import { Component, OnInit } from '@angular/core';
import { TextToSpeech } from '@ionic-native/text-to-speech/ngx';
import { Router } from '@angular/router';
import { DeviceFeedback } from '@ionic-native/device-feedback/ngx';

@Component({
  selector: 'app-sound-test-guide',
  templateUrl: './sound-test-guide.page.html',
  styleUrls: ['./sound-test-guide.page.scss'],
})
export class SoundTestGuidePage implements OnInit {
  disableBtn: any;

  constructor(private tts: TextToSpeech, private router: Router, private deviceFeedback: DeviceFeedback) { }

  ngOnInit() {
    this.disableBtn = true;
    var an = "We will be conducting a short voice interview. Please answer the question with your voice. Please press the button when you are ready."
    this.tts.speak(an).then(() => this.disableBtn = false);
  }

  goNext() {
    this.deviceFeedback.haptic(0);
    this.router.navigate(['/sound-test']);
  }

}
