import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SoundTestGuidePageRoutingModule } from './sound-test-guide-routing.module';

import { SoundTestGuidePage } from './sound-test-guide.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SoundTestGuidePageRoutingModule
  ],
  declarations: [SoundTestGuidePage]
})
export class SoundTestGuidePageModule {}
